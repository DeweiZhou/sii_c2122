// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//CMundo m;
struct mensaje{
   char nombre[10];
   int punto;
};
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
munmap(pdatos,sizeof(DatosMemCompartida));
close(fd_fifo_S_C);
close(fd_fifo_C_S);
 unlink("FIFO_S_C");
 unlink("FIFO_C_S");
 munmap(pdatos,sizeof(DatosMemCompartida));
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	/*
	//FIFO
	struct mensaje m;
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		
		char jugador[]="Jugador 2";
		for(i=0;i<10;i++){
		   m.nombre[i] = jugador[i];
		
		}
		 m.punto =puntos2;
		write(fd, &m, sizeof(struct mensaje));
	
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		char jugador[]="Jugador 1";
		for(i=0;i<10;i++){
		   m.nombre[i] = jugador[i];
		
		}
		 m.punto =puntos1;
		write(fd, &m, sizeof(struct mensaje));
		
	}*/
	
	 //Actualizar los atributos de MundoCliente con los datos recibidos
      	char cad[200];
	read(fd_fifo_S_C,cad,sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
	//terminar partida quien consiga 3 puntos
	if(puntos1==3||puntos2==3)
	    exit(1);
	
	//bot
	pdatos->esfera=esfera;
	pdatos->raqueta1=jugador1;
	//datos.accion=pdatos->accion;
	//printf("esfera.centro.y %f\n",pdatos->esfera.centro.y);
	if(pdatos->accion==1){
	 
	     OnKeyboardDown('w', 0, 0);
	    }
      	if(pdatos->accion==-1){
	
	    OnKeyboardDown('s', 0, 0);
      }
      	if(pdatos->accion==0){
	
	    OnKeyboardDown('0', 0, 0);
      }
      

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
       char cadena[100];
	sprintf(cadena,"%c", key);
	write(fd_fifo_C_S, cadena, sizeof(cadena));
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case '0':jugador1.velocidad.y=0;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
      
}

void CMundo::Init()
{
    
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//FIFO
	//fd=open("FIFO",O_WRONLY);
	
        //FIFO SERVIDOR A CLIENTE
      //  unlink("FIFO_S_C");
       
       if(mkfifo("FIFO_S_C",0666)<0){
            perror("No puede crearse FIFO_S_C");
           }

      if((fd_fifo_S_C=open("FIFO_S_C",O_RDONLY))<0){
              perror("No puede abrirse FIFO_S_C");
           }
	
	//FIFO DE CLIENTE A SERVIDOR
	//unlink("FIFO_C_S");
       
       if(mkfifo("FIFO_C_S",0666)<0){
            perror("No puede crearse FIFO_C_S");
           }

      if((fd_fifo_C_S=open("FIFO_C_S",O_WRONLY))<0){
              perror("No puede abrirse FIFO_C_S");
           }
	
	
        //bot
        int fd2;
        if(fd2<0){
             perror("Error crear o abrir fichero");
        }
        
	fd2= open("bot_jugador1",O_CREAT|O_TRUNC|O_RDWR,0766);
        write(fd2, &datos, sizeof(DatosMemCompartida));
	//ftruncate(fd2,sizeof(DatosMemCompartida));
	
	pdatos=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	close(fd2);
	printf("direccion %p\n",pdatos);
	
}
