# Changelog

## [1.0] - 2021-10-07


### Added
- Añadido changelog

### Changed


## [2.0] - 2021-10-29
### Added
- Añadido movimiento a esfera y raqueta
- Añadido disminución del tamaño de esfera según avanza el juego.
- Creado Readme.md

### Changed
- Eliminado carpeta bin y build del repositorio


## [3.0] - 2021-11-23
### Added
- Creado logger.cpp
- Creado DatosMemCompartida.h
- Creado bot

### Changed
- Modificar código Mundo.h y Mundo.cpp  para poder implementar el programa bot

## [4.0] - 2021-12-08
### Added
- Creado MundoCliente.cpp
- Creado MundoServidor.cpp
- Creado MundoCliente.h
- Creado MundoServidor.h
- Creado Cliente.cpp
- Creado Servidor.cpp

### Changed
- Borrado tenis.cpp
- Borrado Mundo.cpp

